module.exports = function (io, socket, players, player, deckLogic) {

	var checkPoll = function(){
		var returnAnswer = true;
		console.log(players);
			players.forEach(function(player){
				if(!player.voted){
					returnAnswer = false;
				}
			});
			return returnAnswer;
	}

	var checkPollOutcome = function(){	
	var returnAnswer = true;
			players.forEach(function(player){
				if(!player.pollVote){
					returnAnswer = false;
				}
			});
			return returnAnswer;
	}

	var resetPollVote = function(){
		players.forEach(function(player){
				player.voted = false;
			});
	}


	socket.on('shuffleCardsVote',  function(){
			//player.pollVote = true;
			io.sockets.emit('shuffleCardsPoll');
	});
	
	socket.on('shuffleCardsPollAnswer', function(pollAnswer){
			player.pollVote = pollAnswer;
			player.voted = true;
			if(checkPoll()){
					if(checkPollOutcome()){
						io.sockets.emit('votePassed');
						deckLogic.shuffle();
						io.sockets.emit('allCardsMessage', deckLogic.cleanDeck(deckLogic.deck));
					}else{
						io.sockets.emit('voteFailed');
					}	
					resetPollVote();
			}
	});

}

