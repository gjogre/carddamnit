module.exports = function (io, socket, deckLogic) {
		
		var findCardById = function(id) {

			var cardToSend = null;

			deckLogic.deck.forEach(function(cardToFind){

				if(id == cardToFind.id){
					cardToSend = cardToFind; 	
				}	

			});

			return cardToSend;

		}

		


		socket.on('cardMoved', function(card){
				
			var cardToSend = findCardById(card.id);

			if(cardToSend != null){

				cardToSend.x = card.x;
				cardToSend.y = card.y;
				cardToSend.locked = false;
				var clean = deckLogic.cleanCard(cardToSend)
				socket.broadcast.emit('replaceCard', clean);

			}
		});

		socket.on('cardHeld', function(card) {
			
			var cardToSend = findCardById(card.id);

			if(cardToSend != null){

				cardToSend.locked = true;
				socket.broadcast.emit('replaceCard', deckLogic.cleanCard(cardToSend));

			}

		});

		socket.on('cardFlip', function(card) {

			var cardToSend = findCardById(card.id);
			console.log(cardToSend);
			if(cardToSend != null && !cardToSend.locked){

				cardToSend.hide = !cardToSend.hide;

				io.sockets.emit('replaceCard', deckLogic.cleanCard(cardToSend));

			}

		});
}