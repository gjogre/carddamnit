module.exports = function () {
	
	var id = 0;

	var card = function(number, suite, x, y) {
		this.id = id++;
		this.number = number;
		this.suite = suite;		
		this.x = x;
		this.y = y;
		this.locked = false;
	}

	this.deck = [];

	this.cleanCard = function(card) {
		// make copy of a card
		var clean = JSON.parse(JSON.stringify(card));
		// modify properties so we dont uncover secrets
		if(clean.hide) {

			clean.number = 0; 
		}
		return clean;
	}

	this.cleanDeck = function(deck) {
		// make copy of a card
		var clean = JSON.parse(JSON.stringify(deck));
		// modify properties so we dont uncover secrets
		clean.forEach(function(card) {
			if(card.hide) {

				card.number = 0; 
			}

		})
		
		return clean;
	}

	this.resetCards = function(type) {
		
		this.deck = [];

		for(var number = 1; number <= 53; number++) {
			this.deck.push(new card(number,0,number*20, 200));
		}
	}
	
	this.shuffle = function(){	
			var counter = this.deck.length, temp, index;
			
			while(counter > 0){
				index = Math.floor(Math.random() * counter);
				
				counter--;
				
				temp = this.deck[counter];
				this.deck[counter] = this.deck[index];
				this.deck[index] = temp;	
			}
			
			for(var i = 0; i < this.deck.length; i++) {
				this.deck[i].y = 200;
				this.deck[i].x = 20 + (i * 20);
				this.deck[i].hide = true;
			}
	}
};