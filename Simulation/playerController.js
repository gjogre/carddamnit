var Shuffle = require('./modules/shuffle.js');
var Player = require('./modules/player.js');
var CardMovement = require('./modules/cardMovement.js');

var players = [];
var playerId = 0;

module.exports = function (io, socket, deckLogic) {
	var player = new Player(playerId);
	var shuffle = new Shuffle(io, socket, players, player, deckLogic);
	var cardMovement = new CardMovement(io, socket, deckLogic);	
	
	playerId++;
	players.push(player);


	socket.on('disconnect', function(){
		players.splice(players.indexOf(player), 1);
    	console.log('troll disconnected');
 	});
}