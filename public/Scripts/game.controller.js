app.controller('GameController', ['$scope', '$timeout', function($scope, $timeout) {
  $scope.test = "Carddamnit - GAME";
  
  
  var socket = io();

  $scope.cards = [];

  socket.on('allCardsMessage', function(msg) {
  	console.log(msg);
  	$scope.cards = msg;
    if(!$scope.$$phase) {
      $scope.$apply();
    }
  	
  });
  
	$scope.shufflePoll = function(){
		socket.emit('shuffleCardsVote');
	} 
	
	socket.on('shuffleCardsPoll', function(){
		if(confirm('Shuffle cards?')){
			socket.emit('shuffleCardsPollAnswer', true);
		}else{
			socket.emit('shuffleCardsPollAnswer', false);
		}
	});
	
	socket.on('voteFailed', function(){
			alert('poll failed');
	});
  
 socket.on('votePassed', function(){
			alert('poll passed');
	});
	
	socket.on('replaceCard', function(card){
		$scope.cards.forEach(function(cardToReplace){
			if(card.id == cardToReplace.id){
				cardToReplace.x = card.x;
				cardToReplace.y = card.y;
        cardToReplace.number = card.number;

        if(!$scope.$$phase) {
         $scope.$apply();
        } 

        $timeout(function() {

        cardToReplace.locked=card.locked;
        
      }, 200);
			}
		});

	});


  $scope.flip = function(card) {
    console.log("flip")
    socket.emit('flip', card);
  }


	$scope.cardmoved = function(card){

		socket.emit('cardMoved', card);
	}	
	$scope.cardhold = function(card){
    console.log(card.button);
    if(card.button == 0) {
      socket.emit('cardHeld', card);
    } 
    else if(card.button == 2) {
      socket.emit('cardFlip', card);
    }
    
  } 
  $scope.cardFlip = function(card){
    socket.emit('cardFlip', card);
  }
	
	
}])


.directive('draggable', ['$document', function($document) {
  return {
  	restrict: 'A',
  	scope: {
            card: "=",
            mouseupcallback: "&",
            mousedowncallback: "&"                            
        },
    link: function(scope, element, attr) {

      var startX = scope.card.x, startY = scope.card.y, x = scope.card.x, y = scope.card.y;

      element.css({
       position: 'absolute',
       backgroundColor: 'lightgrey',
       cursor: 'pointer',
       width: '142px',
       height: '192px',
       
      });

      $(element).bind("contextmenu", function(event) {
          event.preventDefault();
      });

      element.on('mousedown', function(event) {
        // Prevent default dragging of selected content
        event.preventDefault();

        startX = event.pageX - scope.card.x;
        startY = event.pageY - scope.card.y;
        $document.on('mousemove', mousemove);
        $document.on('mouseup', mouseup);
        scope.card.button = event.button;
        scope.mousedowncallback(scope.card);
      });

      function mousemove(event) {
        y = event.pageY - startY;
        x = event.pageX - startX;
        scope.card.x = x;
        scope.card.y = y;
        element.css({
          'z-index': 2,
          'top': scope.card.y ,
          'left': scope.card.x
        });
      }

      function mouseup() {
      	element.css({
          'z-index': 1
        });
        $document.off('mousemove', mousemove);
        $document.off('mouseup', mouseup);
        scope.mouseupcallback(scope.card);
      }
    }
  };
}]);