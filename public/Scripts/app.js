var app = angular.module('carddamnit', []);


app.config(['$routeProvider', 
	function($routeProvider) {
	    $routeProvider.
	      when('/', {
	        templateUrl: 'Views/menu.html',
	        controller: 'MenuController'
	      }).
	      when('/game', {
	        templateUrl: 'Views/game.html',
	        controller: 'GameController'
	      }).
	      otherwise({
	        redirectTo: '/'
	      });
	   
  }]);

// Draggable directive from: http://docs.angularjs.org/guide/compiler


