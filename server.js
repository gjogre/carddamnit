var express  = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var DeckLogic = require('./Simulation/cardlogic.js');
var PlayerController = require('./Simulation/playerController.js');

var PORT = 3000;

app.use(express.static(__dirname + '/public')); 


var deckLogic = new DeckLogic();

deckLogic.resetCards(0);





io.on('connection', function(socket){
 	console.log('troll connected');

	socket.emit('allCardsMessage', deckLogic.cleanDeck(deckLogic.deck));

	var playerController = new PlayerController(io,socket, deckLogic);



});



http.listen(PORT, function(){
  console.log('listening on port:'+PORT);
});
